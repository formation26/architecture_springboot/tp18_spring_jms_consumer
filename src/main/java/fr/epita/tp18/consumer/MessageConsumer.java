package fr.epita.tp18.consumer;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class MessageConsumer {
	
	
	@JmsListener(destination = "Queue_epita")
	public void receiveMessage(String message) {
		System.out.println(message);
		
	}

}
